import React, { memo } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';

import favicon from '../../favicon.png';

const PageTitle = ({ title }) => {
  const replacedTitle = title.replaceAll('Strapi', '').replaceAll(' - ', '');
  const properTitle = `${replacedTitle} | Static Web Studio`

  return (
      <Helmet title={properTitle} link={[{ rel: 'icon', type: 'image/png', href: favicon }]} />
  )
};

PageTitle.propTypes = {
  title: PropTypes.string.isRequired,
};

export default memo(PageTitle);
