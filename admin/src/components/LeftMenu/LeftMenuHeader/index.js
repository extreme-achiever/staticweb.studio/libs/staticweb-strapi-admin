import React from 'react';
import {Link} from 'react-router-dom';

import Wrapper from './Wrapper';

const LeftMenuHeader = () => (
    <Wrapper>
        <Link style={{
            "align-items": "center",
            "display": "inline-flex",
            "width": "100%"
        }} to="/" className="leftMenuHeaderLink">
            <span style={{flex: 1}} className="projectName"/>
            <span style={{
                color: "white",
                width: "100%",
                flex: 5,
            }}>Static Web Studio</span>
        </Link>
    </Wrapper>
);

export default LeftMenuHeader;
